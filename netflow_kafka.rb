#!/usr/bin/env ruby

require 'rubygems'

require 'poseidon'
require 'eventmachine'
require 'optparse'

require File.join(File.dirname(__FILE__),'lib', 'netflow_collector')

if ARGV.length != 3
   puts "usage: netflow_kafka.rb broker producer_name topic"
   exit
end


EM.run do

   broker = ARGV[0]
   producer_name = ARGV[1]
   topic = ARGV[2]

   producer = Poseidon::Producer.new([broker], producer_name)

   EM.open_datagram_socket('0.0.0.0', 2055, NetflowCollector, producer, topic)

   Signal.trap("INT")  { EM.stop }
   Signal.trap("TERM") { EM.stop }

end
