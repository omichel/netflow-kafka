
require 'bindata'

class NetflowHeader < BinData::Record
   endian :big
   uint16 :version
   uint16 :flow_records
end
