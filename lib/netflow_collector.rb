
require 'socket'
require 'ipaddr'
require 'json'
require 'poseidon'

require File.join(File.dirname(__FILE__), 'netflow5_packet')
require File.join(File.dirname(__FILE__), 'netflow_header')

class NetflowCollector < EM::Connection

   def initialize(producer, topic)
      @kafka_producer = producer
      @topic = topic
   end

   def receive_data(data)

      port, ip = Socket.unpack_sockaddr_in(get_peername)
      header = NetflowHeader.read(data)


      if header.version == 5

         flowset = Netflow5Packet.read(data)

         flowset.records.each do |flow|

            hash = {
               exporter: "#{ip}:#{port}",
               unix_sec: flowset.unix_sec,
               unix_nsec: flowset.unix_nsec,
               seq: flowset.flow_seq_num += 1,
               iface_in: flow.iface_in,
               iface_out: flow.iface_out,
               srcaddr: IPAddr.new(flow.srcaddr, Socket::AF_INET).to_s,
               dstaddr: IPAddr.new(flow.dstaddr, Socket::AF_INET).to_s,
               proto:   flow.proto,
               srcport: flow.srcport,
               dstport: flow.dstport,
               packets: flow.packets,
               octets:  flow.octets
            }

            kafka_msg = Poseidon::MessageToSend.new(@topic, hash.to_json)
            @kafka_producer.send_messages([kafka_msg])

            puts hash.to_json

         end
      end

   end
end
