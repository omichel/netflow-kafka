Netflow Kafka Producer
======================

(c) 2014 Oliver Michel [http://ngn.cs.colorado.edu/~oliver](http://ngn.cs.colorado.edu/~oliver)

Simple Ruby script that listens for incoming NetFlow packages on udp:2055 and pipes them into Kafka as JSON objects.

Install
-------

Download and execute `bundle install`. Requires rubygems and Ruby Version > 2.1.

Usage
-----

```
netflow_kafka.rb kafka-broker producer-name topic
```
